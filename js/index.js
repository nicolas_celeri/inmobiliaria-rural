
    $(function () {
       $("[data-toggle='popover']").popover();
       
       
       $('.carousel').carousel({
            interval: 800
       });


       $('#exampleModal1').on('show.bs.modal', function (e) {
           console.log("el modal se muestra");

           $('#btn-jumbo').removeClass('btn-success');
           $('#btn-jumbo').addClass('btn-danger');
           $('#btn-jumbo').prop('disabled',true);
         })

       $('#exampleModal1').on('shown.bs.modal', function (e) {
           console.log("el modal se motro");
       })

       $('#exampleModal1').on('hide.bs.modal', function (e) {
           console.log("el modal se oculta");
       })

       $('#exampleModal1').on('hidden.bs.modal', function (e) {
           console.log("el modal se oculto");
           $('#btn-jumbo').removeClass('btn-danger');
           $('#btn-jumbo').addClass('btn-success');
           $('#btn-jumbo').prop('disabled',false);
       })
   })
   